# Assembly of jetson robot

Beginning of project, where im spacing the diffrent parts:
![beginning](scr/beginning.jpg)

Adding Nvidia board to the main robot-board:
![brain](scr/brain.jpg)

Moving the hardware inside the body:
![tobody](scr/adding2body.jpg)

Installing software:
![software](scr/software.jpg)

Assembling the arm with camera:
![arm](scr/arm.jpg)

Robot fully assembled!
![final product](scr/final.jpg)

Big smile:
![picture with smile](scr/smile.jpg)